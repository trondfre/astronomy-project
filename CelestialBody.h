#pragma once

#include <armadillo>

using namespace arma;

class CelestialBody
{
public:
  vec3 position;
  vec3 velocity;
  vec3 prevposition; //needed for verletsolver
  vec3 force;
  double mass;
  double kineticenergy;
  double potentialenergy;

  CelestialBody(vec position, vec velocity, double mass);
  CelestialBody(double x, double y, double z, double vx, double vy, double vz, double mass);
  void resetForce();
  double kineticEnergy();
  vec3 angularMomentum();
};
