#include "CelestialBody.h"

CelestialBody::CelestialBody(vec pos, vec vel, double mass_) {
  position = pos;
  velocity = vel;
  mass = mass_;
}

CelestialBody::CelestialBody(double x, double y, double z, double vx, double vy, double vz, double mass_) {

  position(0) = x; position(1) = y; position(2) = z;
  velocity(0) = vx; velocity(1) = vy; velocity(2) = vz;
  mass = mass_;
}

void CelestialBody::resetForce() {
  force.zeros();
}

double CelestialBody::kineticEnergy() {
  return 0.5*mass*dot(velocity,velocity);
}

vec3 CelestialBody::angularMomentum() {
  return mass*cross(position, velocity);
}
