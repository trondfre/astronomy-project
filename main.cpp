#include <iostream>
#include <cstdio>
#include <cmath>

#include "CelestialBody.h"
#include "SolarSystem.h"
#include "gaussiandeviate.h"

using namespace std;

int main()
{
  SolarSystem mySystem;

  int N = 150;
  mySystem.addNBodies(N);

  cout << "number of bodies: " << mySystem.numberofBodies() << endl;
  int n = 100000;
  double endtime = 0.5, dt;

  dt = endtime/(n+1.0);


    for (int j=0; j<mySystem.numberofBodies(); j++) {
      CelestialBody &thisBody = mySystem.bodies[j];
      thisBody.prevposition = thisBody.position - dt*thisBody.velocity;
    }

  mySystem.RK4solve(n, endtime);

  FILE *output_file;
  output_file = fopen("posT=0p5N=150.dat", "w");
  for (int i=0; i<mySystem.numberofBodies(); i++) {
      CelestialBody &thisBody = mySystem.bodies[i];
      fprintf(output_file, "%12.5E %12.5E %12.5E ", thisBody.position(0), thisBody.position(1), thisBody.position(2));
      fprintf(output_file, "\n");
  }
  fclose(output_file);

  mySystem.RK4solve(n, 0.5);

  output_file = fopen("posT=1p0N=150.dat", "w");
  for (int i=0; i<mySystem.numberofBodies(); i++) {
      CelestialBody &thisBody = mySystem.bodies[i];
      fprintf(output_file, "%12.5E %12.5E %12.5E ", thisBody.position(0), thisBody.position(1), thisBody.position(2));
      fprintf(output_file, "\n");
  }
  fclose(output_file);

  mySystem.RK4solve(n, 1);

  output_file = fopen("posT=2p0N=150.dat", "w");
  for (int i=0; i<mySystem.numberofBodies(); i++) {
      CelestialBody &thisBody = mySystem.bodies[i];
      fprintf(output_file, "%12.5E %12.5E %12.5E ", thisBody.position(0), thisBody.position(1), thisBody.position(2));
      fprintf(output_file, "\n");
  }
  fclose(output_file);

  mySystem.RK4solve(n, 1);

  output_file = fopen("posT=3p0N=150.dat", "w");
  for (int i=0; i<mySystem.numberofBodies(); i++) {
      CelestialBody &thisBody = mySystem.bodies[i];
      fprintf(output_file, "%12.5E %12.5E %12.5E ", thisBody.position(0), thisBody.position(1), thisBody.position(2));
      fprintf(output_file, "\n");
  }
  fclose(output_file);

  double totalEk = 0, totalEp = 0;
  cout << "lost energy: " << mySystem.lostEnergy << endl;
  for (int i=0; i<mySystem.numberofBodies(); i++) {
      CelestialBody &thisBody = mySystem.bodies[i];
      totalEk += thisBody.kineticenergy;
      totalEp += thisBody.potentialenergy;
  }

  cout << "average kinetic energy: " << totalEk/mySystem.numberofBodies() << endl;
  cout << "average potential energy: " << 0.5*totalEp/mySystem.numberofBodies() << endl;

  vec density = mySystem.radialdensity(20);
  
  output_file = fopen("radialdensityN=150.dat", "w");
  for (int i=0; i<20; i++) {
    fprintf(output_file, "%12.5E", density(i));
    fprintf(output_file, "\n");
  }
  fclose(output_file);

  return 0;
}
