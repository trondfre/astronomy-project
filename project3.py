from matplotlib.pyplot import *
from numpy import *
from mpl_toolkits.mplot3d import Axes3D

data = loadtxt('positions.dat')
n = int(data.shape[1]/3.)

fig = figure()
ax = fig.add_subplot(111, projection='3d')
for i in range(0,n):
    ax.plot(data[:,3*i],data[:,3*i+1],data[:,3*i+2])
    hold('on')
show()
