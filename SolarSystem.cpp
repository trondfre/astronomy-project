#include "SolarSystem.h"

SolarSystem::SolarSystem() {
}

void SolarSystem::addCelestialBody(CelestialBody newBody) {
  bodies.push_back(newBody);
}

void SolarSystem::addNBodies(int N) {
  long k = -1;
  double u, v, w, x, y, z, R0 = 20.0, r, theta, phi, m;

  for (int i=0; i<N; i++) {
    u = ran2(&k);
    v = ran2(&k);
    w = ran2(&k);

    r = R0*pow(u,1.0/3);
    theta = acos(1 - 2*v);
    phi = 2*M_PI*w;

    x = r*sin(theta)*cos(phi);
    y = r*sin(theta)*sin(phi);
    z = r*cos(theta);

    m = gaussian_deviate(&k) + 10;

    CelestialBody X(x, y, z, 0, 0, 0, m);
    addCelestialBody(X);
  }
}

mat SolarSystem::calculateForces(vec B) {
  vec accelAndvel = zeros(6*numberofBodies());
  double x2, y2, z2, dx, dy, dz, dr, Gdr;
#pragma omp parallel for shared(accelAndvel) private(x2,y2,z2,dx,dy,dz,dr,Gdr) num_threads(4)
  for (int i=0; i<numberofBodies(); i++) {
      CelestialBody &body1 = bodies[i];
    double x1 = B(6*i); double y1 = B(6*i+1); double z1 = B(6*i+2);
    accelAndvel(6*i) = B(6*i+3); //set velocities v_i
    accelAndvel(6*i+1) = B(6*i+4);
    accelAndvel(6*i+2) = B(6*i+5);
    for (int j=i+1; j<numberofBodies(); j++) {
      CelestialBody &body2 = bodies[j];
      x2 = B(6*j); y2 = B(6*j+1); z2 = B(6*j+2);
      dx = x2 - x1; dy = y2 - y1; dz = z2 - z1;
      dr = sqrt(dx*dx + dy*dy + dz*dz);

      Gdr = (M_PI*M_PI*20*20*20/(8*10*100))/((dr*dr + 1e-3)*dr);
      accelAndvel(6*i+3) += Gdr*body2.mass*dx; 
      accelAndvel(6*i+4) += Gdr*body2.mass*dy; 
      accelAndvel(6*i+5) += Gdr*body2.mass*dz;

      accelAndvel(6*j+3) -= Gdr*body1.mass*dx; 
      accelAndvel(6*j+4) -= Gdr*body1.mass*dy; 
      accelAndvel(6*j+5) -= Gdr*body1.mass*dz;
    }
  }
   return accelAndvel;
}

int SolarSystem::numberofBodies() {
  return bodies.size();
}

void SolarSystem::potentialEnergy() {
  for (int i=0; i<numberofBodies(); i++) {
    CelestialBody &thisBody = bodies[i];
    thisBody.potentialenergy = 0;
  }

  for (int i=0; i<numberofBodies(); i++) {
    CelestialBody &body1 = bodies[i];
    for (int j=i+1; j<numberofBodies(); j++) {
	CelestialBody &body2 = bodies[j];
	
	double dx = body2.position(0) - body1.position(0);
	double dy = body2.position(1) - body1.position(1);
	double dz = body2.position(2) - body1.position(2);
	double dr = sqrt(dx*dx + dy*dy + dz*dz);


	body1.potentialenergy += -(M_PI*M_PI*20*20*20/(8*10*100))*body1.mass*body2.mass/dr;
	body2.potentialenergy += -(M_PI*M_PI*20*20*20/(8*10*100))*body1.mass*body2.mass/dr;
    }
  }
}

vec SolarSystem::radialdensity(int n) {
  double R0 = 20.0;
  double rstep = R0/n;
  vec density = zeros(n);

  for (int i=0; i<numberofBodies(); i++) {
    CelestialBody &thisBody = bodies[i];

    double x = thisBody.position(0);
    double y = thisBody.position(1);
    double z = thisBody.position(2);
    double r = sqrt(x*x + y*y + z*z);
    
    for (int j=0; j<n; j++) {
      if (r > j*rstep && r <= (j+1)*rstep) {
	double V = 4.0/3*M_PI*rstep*rstep*rstep*(3*j*(j + 1) + 1);
	density(j) += 1.0/V;
      }
    }
  }

  return density;
}
void SolarSystem::removeFreeBodies() {
    for (int i=0; i<numberofBodies(); i++) {
      CelestialBody &thisBody = bodies[i];
      double x = thisBody.position(0);
      double y = thisBody.position(1);
      double z = thisBody.position(2);
      double r = sqrt(x*x + y*y + z*z);

      if (thisBody.kineticenergy > -thisBody.potentialenergy && r > 20.0) {
	cout << "Removed an object!" << endl;
	lostEnergy += thisBody.kineticenergy + thisBody.potentialenergy;
	bodies.erase(bodies.begin() + i);
	i = i-1;
	cout << "Number of objects: " << numberofBodies() << endl;
      }
    }
}

void SolarSystem::RK4solve(int n, double endtime) {
  double dt = endtime/(n+1.0);
  double timeMoved = 0;
  timeMoved += dt;

  //FILE *output_file;
  //output_file = fopen("positions.dat", "w");

  while (timeMoved <= endtime) {
    vec A = zeros(6*numberofBodies());
    for (int i=0; i<numberofBodies(); i++) {
      CelestialBody &thisBody = bodies[i];
      A(6*i) = thisBody.position(0); A(6*i+1) = thisBody.position(1); A(6*i+2) = thisBody.position(2);
      A(6*i+3) = thisBody.velocity(0); A(6*i+4) = thisBody.velocity(1); A(6*i+5) = thisBody.velocity(2);
    }

    vec k1 = dt*calculateForces(A);
    vec k2 = dt*calculateForces(A + c1*k1);
    vec k3 = dt*calculateForces(A + c2*k1 + c3*k2);
    vec k4 = dt*calculateForces(A + c4*k1 + c5*k2 + c6*k3);
    vec k5 = dt*calculateForces(A + c7*k1 + c8*k2 + c9*k3 + c10*k4);
    vec k6 = dt*calculateForces(A + c11*k1 + c12*k2 + c13*k3 + c14*k4 + c15*k5);

    vec A4 = A + c16*k1 + c17*k3 + c18*k4 + c19*k5;
    vec A5 = A + c20*k1 + c21*k3 + c22*k4 + c23*k5 + c24*k6;

    vec diff = abs(A5 - A4);
    double maxdiff = max(diff) + 1e-16;

    double totalEnergy = 0;
    potentialEnergy();
    for (int i=0; i<numberofBodies(); i++) {
      CelestialBody &thisBody = bodies[i];

      //fprintf(output_file, "%12.5E %12.5E %12.5E ", thisBody.position(0), thisBody.position(1), thisBody.position(2));
      
      thisBody.kineticenergy = thisBody.kineticEnergy();
      totalEnergy += thisBody.kineticenergy;
      totalEnergy += thisBody.potentialenergy/2;

      thisBody.position(0) = A4(6*i); thisBody.position(1) = A4(6*i+1); thisBody.position(2) = A4(6*i+2);
      thisBody.velocity(0) = A4(6*i+3); thisBody.velocity(1) = A4(6*i+4); thisBody.velocity(2) = A4(6*i+5);
    }
    //fprintf(output_file, "\n");

    cout << "total energy = " << totalEnergy << endl;
    cout << "time moved = " << timeMoved << endl;
    cout << "dt = " << dt << endl;
    cout << "number of bodies:  " << numberofBodies() << endl;

    removeFreeBodies();
    
    timeMoved += dt;
    dt = dt*pow(1e-3*dt/(2*maxdiff),0.25);
    if (dt < 1e-5) {
      dt = 1e-5;
    }
  }
  //fclose(output_file);
}

void SolarSystem::verletsolve(double dt) {
  double originaldt = dt;

  double timeMoved = 0;
  timeMoved += dt;

  while (timeMoved <= originaldt) {
    for (int i=0; i<numberofBodies(); i++) {
      CelestialBody &thisBody = bodies[i];
      //calculate next position
      vec3 newpos = 2*thisBody.position - thisBody.prevposition + dt*dt*thisBody.force/thisBody.mass;
      thisBody.velocity = (newpos - thisBody.prevposition)/(2*dt);
      thisBody.prevposition = thisBody.position;
      thisBody.position = newpos;
      thisBody.resetForce();
    }

    timeMoved += dt;
  }
}
