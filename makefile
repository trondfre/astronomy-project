GCC= g++ -Wall -fopenmp -std=c++11
PROG= main

${PROG} :        ${PROG}.o CelestialBody.o SolarSystem.o gaussiandeviate.o
		 ${GCC} ${PROG}.o CelestialBody.o SolarSystem.o gaussiandeviate.o -lm -o ${PROG}.x -larmadillo -llapack -lblas

${PROG}.o : 	 ${PROG}.cpp
		 ${GCC} -c ${PROG}.cpp

CelestialBody.o : CelestialBody.cpp CelestialBody.h
		 ${GCC} -c CelestialBody.cpp

SolarSystem.o : CelestialBody.h gaussiandeviate.h SolarSystem.cpp SolarSystem.h
		${GCC} -c SolarSystem.cpp

gaussiandeviate.o : gaussiandeviate.cpp gaussiandeviate.h
		${GCC} -c gaussiandeviate.cpp

clean :
		rm *.o *.x
