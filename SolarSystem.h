#pragma once

#include "CelestialBody.h"
#include "gaussiandeviate.h"
#include <vector>
#include <omp.h>

using std::vector;

class SolarSystem
{
 private:
  double c1 = 1.0/4;
  double c2 = 3.0/32;
  double c3 = 9.0/32;
  double c4 = 1932.0/2197;
  double c5 = -7200.0/2197;
  double c6 = 7296.0/2197;
  double c7 = 439.0/216;
  double c8 = -8;
  double c9 = 3680.0/513;
  double c10 = -845.0/4104;
  double c11 = -8.0/27;
  double c12 = 2;
  double c13 = -3544.0/2565;
  double c14 = 1859.0/5104;
  double c15 = -11.0/40;
  double c16 = 25.0/216;
  double c17 = 1408.0/2565;
  double c18 = 2197.0/4101;
  double c19 = -1.0/5;
  double c20 = 16.0/135;
  double c21 = 6656.0/12825;
  double c22 = 28561.0/56430;
  double c23 = -9.0/50;
  double c24 = 2.0/55;

 public:
  vector<CelestialBody> bodies;
  double lostEnergy = 0;

  SolarSystem();
  void addCelestialBody(CelestialBody newBody);
  void addNBodies(int N);
  mat calculateForces(vec B);
  int numberofBodies();
  void potentialEnergy();
  vec radialdensity(int n);
  void removeFreeBodies();
  void RK4solve(int n, double endtime);
  void verletsolve(double dt);
};
